export const tools = [
	{
		type: 'drag',
		icon: 'drag',
		name: '拖拽'
	},
	{
		type: 'connection',
		icon: 'fork',
		name: '连线'
	},
	{
		type: 'zoom-in',
		icon: 'zoom-in',
		name: '放大'
	},
	{
		type: 'zoom-out',
		icon: 'zoom-out',
		name: '缩小'
	}
];

export const commonNodes = [
	{
		type: 'start', 
		nodeName: '开始', 
		icon: 'StartIcon'
	},
	{
		type: 'end', 
		nodeName: '结束', 
		icon: 'EndIcon'
	},
	{
		type: 'doService',
		nodeName: '适配器服务',
		icon: 'CommonIcon'
	},
	{
		type: 'doSafService',
		nodeName: 'SAF服务',
		icon: 'FreedomIcon'
	},
	{
		type: 'SQL',
		nodeName: 'SQL服务',
		icon: 'GatewayIcon'
	},
	{
		type: 'data',
		nodeName: '数据处理',
		icon: 'EventIcon'
	}
];

export const highNodes = [

];

export const laneNodes = [

];
