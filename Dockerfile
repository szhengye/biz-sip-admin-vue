FROM nginx
MAINTAINER 18601781218@163.com
VOLUME /tmp
ENV LANG en_US.UTF-8
RUN echo "server {  \
                  listen       3000; \
                  location ^~ /jeecg-boot { \
                      add_header 'Access-Control-Allow-Origin' \$http_origin; \
                      add_header 'Access-Control-Allow-Credentials' 'true'; \
                      add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE, PUT'; \
                      add_header 'Access-Control-Allow-Headers' 'Origin,X-Requested-With,Content-Type,Accept,Authorization'; \
                      add_header 'Access-Control-Expose-Headers' 'Cache-Control, Content-Language, Content-Type, Expires, Last-Modified, Pragma'; \
                      if (\$request_method = 'OPTIONS' ) { \
			                   add_header 'Access-Control-Max-Age' 1728000; \
			                   add_header 'Content-Type' 'text/plain; charset=utf-8'; \
			                   add_header 'Content-Length' 0; \
			                   return 204; \
                      } \
                      proxy_pass              http://bizsip-admin-app:8080/jeecg-boot/; \
                      proxy_set_header        Host \$host; \
                      proxy_set_header        X-Real-IP \$remote_addr; \
                      proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for; \
                      proxy_set_header X-Forwarded-Proto \$scheme; \
                      proxy_connect_timeout 600; \
                      proxy_read_timeout 600; \
                  } \
                  #解决Router(mode: 'history')模式下，刷新路由地址不能找到页面的问题 \
                  location / { \
                     root   /var/www/html/; \
                      index  index.html index.htm; \
                      if (!-e \$request_filename) { \
                          rewrite ^(.*)\$ /index.html?s=\$1 last; \
                          break; \
                      } \
                  } \
                  access_log  /var/log/nginx/access.log ; \
              } " > /etc/nginx/conf.d/default.conf \
    &&  mkdir  -p  /var/www \
    &&  mkdir -p /var/www/html

ADD dist/ /var/www/html/
EXPOSE 80
EXPOSE 443